# Hike67

## Description

Hike67 is a web application built with Django that allows users to track their progress in hiking the 67 4,000-plus-foot peaks in New England. Additionally, it provides a platform for users to maintain a blog of their hiking experiences. The application makes use of a virtual environment to manage dependencies and ensure consistent behavior across different environments.

## Features

- User Registration and Authentication: Users can create an account and log in to the application to access personalized features.
- Peak Tracking: Users can track their progress in hiking the 67 4,000-plus-foot peaks in New England. They can mark peaks as completed, record the date of completion, and keep track of their overall progress.
- Peak Details: The application provides detailed information about each peak, including its name, elevation, location, and difficulty level.
- Blogging: Users can journal to document their hiking experiences. They can write about their favorite peaks, and provide tips and recommendations to fellow hikers.


## Installation

To set up the Hike67 application, follow these steps:

1. Clone the repository from GitHub:

```bash
git clone https://github.com/your-username/hike67.git
```

2. Activate a virtual environment in the directory

```bash
cd hike67
python3 -m venv myenv
```

3. Activate the virtual environment
Windows(PowerShell):

```bash
myenv\Scripts\Activate.ps1
```

macOS/Linux:

```bash
source myenv/bin/activate
```

4. Install the project dependencies

```bash
pip install -r requirements.txt
```

5. Set up the database

```bash
python manage.py migrate
```

6. Start the development server

```bash
python manage.py runserver
```
