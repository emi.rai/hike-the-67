from django.urls import path

from accounts.views import login_view, logout_user, signup


urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup, name="signup"),
]
