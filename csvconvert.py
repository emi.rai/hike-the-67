from hikethe67.wsgi import *
import csv

from the67.models import FourKFooter


with open("4000footers.csv", newline="") as file:
    raw_data = csv.reader(file)
    table = list(raw_data)

    for i in range(1, len(table)):
        fourk = FourKFooter(
            rank = table[i][0],
            name = table[i][1],
            state_rank = table[i][2],
            elevation = table[i][3],
        )
        fourk.save()
