from django.apps import AppConfig


class HikelogConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "hikelog"
