from django import forms
from django.forms import ModelForm

from hikelog.models import HikeLog


class HikeLogForm(ModelForm):
    class Meta:
        model = HikeLog
        fields = [
            "name",
            "rank",
            "elevation",
            "date_hiked",
            "description",
        ]
