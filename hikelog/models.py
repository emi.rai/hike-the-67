from django.db import models
from django.contrib.auth.models import User


class HikeLog(models.Model):
    name = models.CharField(max_length=200)
    rank = models.PositiveSmallIntegerField(null=True, blank=True)
    state_rank = models.CharField(max_length=10, null=True, blank=True)
    elevation = models.PositiveSmallIntegerField(null=True, blank=True)
    date_hiked = models.DateTimeField(null=True, blank=True)
    description = models.TextField()
    hiker = models.ForeignKey(
        User,
        related_name="hikelogs",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
