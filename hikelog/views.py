from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from hikelog.forms import HikeLogForm


@login_required
def log(request):
    if request.method == "POST":
        form = HikeLogForm(request.POST)
        if form.is_valid():
            hike = form.save(False)
            hike.hiker = request.user
            hike.save()
            return redirect("")
