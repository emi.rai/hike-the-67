from django.contrib import admin

from the67.models import FourKFooter


@admin.register(FourKFooter)
class FourKFooterAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "rank",
        "name",
        "state_rank",
        "elevation",
        "hiker",
        "is_complete",
    )
