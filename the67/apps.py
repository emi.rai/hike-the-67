from django.apps import AppConfig


class The67Config(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "the67"
