from django.db import models
from django.contrib.auth.models import User


class FourKFooter(models.Model):
    rank = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=150)
    state_rank = models.CharField(max_length=10, null=True)
    elevation = models.PositiveSmallIntegerField(null=True)
    is_complete = models.BooleanField(default=False)
    date_hiked = models.DateTimeField(null=True, blank=True)
    hiker = models.ForeignKey(
        User,
        related_name="fourkfooters",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return f"{self.name}, {self.state_rank}"
