from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.decorators import login_required

from the67.models import FourKFooter


def home(request):
    return render(request, "the67/home.html")


def search(request):
    if request.method == "POST":
        searched = request.POST.get("searched")
        fourks = FourKFooter.objects.filter(
            Q(name__contains=searched)
            | Q(state_rank__contains=searched)
            | Q(rank__contains=searched)
        )
        context = {
            "searched": searched,
            "fourks": fourks,
        }
        return render(request, "the67/search.html", context)
