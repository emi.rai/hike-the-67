from django.contrib import admin

from tracker.models import HikeLog

@admin.register(HikeLog)
class HikeLogAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "mountain",
        "start",
        "finish",
        "miles",
        "ascent",
        "mood",
        "conditions",
        "log",
    )
