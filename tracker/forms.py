from django import forms
from emoji_picker.widgets import EmojiPickerTextInputAdmin

from datetime import datetime

from the67.models import FourKFooter
from tracker.models import HikeLog

now = datetime.now().year


class AddFourKForm(forms.ModelForm):
    class Meta:
        model = FourKFooter
        fields = [
            "date_hiked",
            "is_complete",
        ]
        widgets = {
            "date_hiked": forms.widgets.SelectDateWidget(
                attrs={"type": "date"}, years=range(now - 10, now)
            ),
        }


class HikeLogForm(forms.ModelForm):
    class Meta:
        model = HikeLog
        fields = [
            "mountain",
            # "start",
            # "finish",
            "miles",
            "ascent",
            # "mood",
            "conditions",
            "log",
        ]

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['mood'].widget
       