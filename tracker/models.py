from django.db import models
from django.contrib.auth.models import User



class HikeLog(models.Model):
    mountain = models.CharField(max_length=200)
    start = models.CharField(max_length=200, null=True, blank=True)
    finish = models.CharField(max_length=200, null=True, blank=True)
    miles = models.DecimalField(max_digits=4, decimal_places=1, null=True)
    ascent = models.PositiveSmallIntegerField(null=True)
    mood = models.CharField(max_length=250, null=True, blank=True)
    conditions = models.CharField(max_length=250, null=True)
    log = models.TextField()
    hiker = models.ForeignKey(
        User,
        related_name="hikelog",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return f"{self.mountain}, {self.hiker}"