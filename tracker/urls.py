from django.urls import path

from tracker.views import (
    show_67_list,
    add_67,
    show_my_fourks,
    edit_my_fourks,
    log,
    my_log,
    edit_log,
)

urlpatterns = [
    path("the67/", show_67_list, name="the67"),
    path("<int:id>/add_67/", add_67, name="add_67"),
    path("my_4ks", show_my_fourks, name="my_4ks"),
    path("<int:id>/edit", edit_my_fourks, name="edit"),
    path("log/", log, name="log"),
    path("mylog/", my_log, name="my_log"),
    path("<int:id>/edit_log/", edit_log, name="edit_log"),
]
