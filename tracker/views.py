from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from the67.models import FourKFooter
from tracker.models import HikeLog
from tracker.forms import AddFourKForm, HikeLogForm


def show_67_list(request):
    fourks = FourKFooter.objects.filter(hiker=None).order_by("rank").values()
    context = {
        "fourks": fourks,
    }
    return render(request, "tracker/the67.html", context)


@login_required
def add_67(request, id):
    fourk = get_object_or_404(FourKFooter, id=id)
    if request.method == "POST":
        form = AddFourKForm(request.POST)
        if form.is_valid():
            fourkform = form.save(commit=False)
            fourkform.hiker = request.user
            fourkform.name = fourk.name
            fourkform.rank = fourk.rank
            fourkform.elevation = fourk.elevation
            fourkform.state_rank = fourk.state_rank
            fourkform.save()
            return redirect("my_4ks")
    else:
        form = AddFourKForm()  # initial={"rank": fourk.rank}
    context = {
        "fourk": fourk,
        "form": form,
    }
    return render(request, "tracker/add_67.html", context)


@login_required
def show_my_fourks(request):
    fourks = FourKFooter.objects.filter(hiker=request.user)
    context = {
        "fourks": fourks,
    }
    return render(request, "tracker/my_4ks.html", context)


@login_required
def edit_my_fourks(request, id):
    fourk = get_object_or_404(FourKFooter, id=id)
    if request.method == "POST":
        form = AddFourKForm(request.POST, instance=fourk)
        if form.is_valid():
            form.save()
            return redirect("my_4ks")
    else:
        form = AddFourKForm()
    context = {
        "fourk": fourk,
        "form": form,
    }
    return render(request, "tracker/edit.html", context)

@login_required
def log(request):
    if request.method == "POST":
        form = HikeLogForm(request.POST)
        if form.is_valid():
            log = form.save(False)
            log.hiker = request.user
            log.save()
            return redirect("my_log")
    else:
        form = HikeLogForm()
    context = {
        "form": form,
    }
    return render(request, "tracker/log.html", context)


@login_required
def my_log(request):
    logs = HikeLog.objects.filter(hiker=request.user)
    context = {
        "logs": logs,
    }
    return render(request, "tracker/mylog.html", context)


@login_required
def edit_log(request, id):
    log = get_object_or_404(HikeLog, id=id)
    if request.method == "POST":
        form = HikeLogForm(request.POST, instance=log)
        if form.is_valid():
            log = form.save(False)
            return redirect("my_log")
    else:
        form = HikeLogForm(instance=log)
    context = {
        "log": log,
        "form": form,
    }
    return render(request, "tracker/edit_log.html", context)